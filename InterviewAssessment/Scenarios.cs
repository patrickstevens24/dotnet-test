﻿using System;
using System.Collections.Generic;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str)
        {
            char[] charArray = str.ToCharArray();

            Array.Reverse(charArray);

            return new string(charArray);
        }

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 0; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node)
        {
            string nodeText = node.Text;

            if (node.GetType() == typeof(Tree))
            {
                Tree myTree = (Tree)node;
                if (myTree.Children != null)
                {
                    foreach (Node child in myTree.Children)
                    {
                        nodeText += $"-{Scenario4(child)}";
                    }
                }
            }
            return nodeText;
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
